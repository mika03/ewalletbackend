package com.fintech.ewalletbackend.utils;

import java.security.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class KeyPairUtil {
    public static Map<String, String> generateKey() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();
        String publicKeyResponse = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        String privateKeyResponse = Base64.getEncoder().encodeToString(privateKey.getEncoded());
        Map<String, String> response = new HashMap<>();
        response.put("publicKey", publicKeyResponse);
        response.put("privateKey", privateKeyResponse);
        return response;
    }
}
