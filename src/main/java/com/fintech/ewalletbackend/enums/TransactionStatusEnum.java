package com.fintech.ewalletbackend.enums;

public enum TransactionStatusEnum {
    SUCCESS, FAIL, PENDING
}
