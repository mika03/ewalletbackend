package com.fintech.ewalletbackend.enums;

public enum BlockEnum {
    UNBLOCKED, BLOCKED
}
