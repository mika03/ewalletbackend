package com.fintech.ewalletbackend.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {
    // Common
    FAIL("400", "FAIL", "Failure data!"),
    SUCCESS("200", "SUCCESS", "Successful data!"),

    // Data
    DATA_EXIST("400", "DATA_EXIST", "Data exists in the system!"),
    DATA_NOT_EXIST("400", "DATA_NOT_EXIST", "Data does not exist in the system!"),
    NOT_DELETED("400", "NOT_DELETED", "You can not delete this data!"),

    // Account
    EMAIL_PASSWORD_INVALID("400", "EMAIL_PASSWORD_INVALID", "Email or Password Invalid!"),
    ACCOUNT_BLOCKED("400", "ACCOUNT_BLOCKED", "Account is blocked!"),
    ACCOUNT_NOT_ACTIVE("400", "ACCOUNT_NOT_ACTIVE", "Account is not actived!"),
    UNAUTHORIZED("400", "UNAUTHORIZED", "Unauthorized"),
    NOT_ACCESSED_ACCOUNT("400", "NOT_ACCESSED_ACCOUNT", "You do not access this account!");


    private String code;
    private String status;
    private String message;

    ResultEnum(String code, String status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }
}
