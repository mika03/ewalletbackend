package com.fintech.ewalletbackend.enums;

public enum ConfirmedTransactionEnum {
    UNCONFIRMED, CONFIRMED
}
