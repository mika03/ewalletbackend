package com.fintech.ewalletbackend.enums;

public enum StatusEnum {
    ACTIVE, INACTIVE, PENDING
}
