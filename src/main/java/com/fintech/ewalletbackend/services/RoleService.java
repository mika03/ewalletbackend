package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.RoleRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface RoleService {
    ResultResponseDTO create(RoleRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO update(String id, RoleRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO list(String name, Integer page, Integer pageSize, UserPrincipal currentUser);
    ResultResponseDTO info(String id, UserPrincipal currentUser);
    ResultResponseDTO delete(String id, UserPrincipal currentUser);
}
