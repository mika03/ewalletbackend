package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.SystemPermissionRequestDTO;
import com.fintech.ewalletbackend.DTOs.request.SystemPermissionUpdateRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface SystemPermissionService {
    ResultResponseDTO create(SystemPermissionRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO update(String id, SystemPermissionUpdateRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO info(String id, UserPrincipal currentUser);

    ResultResponseDTO delete(String id, UserPrincipal currentUser);

    ResultResponseDTO list(String roleId, String status, Integer page, Integer pageSize, UserPrincipal currentUser);
}
