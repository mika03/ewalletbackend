package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.BlockEmailRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface BlockEmailService {
    ResultResponseDTO create(BlockEmailRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO update(String id, BlockEmailRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO info(String id, UserPrincipal currentUser);
    ResultResponseDTO delete(String id, UserPrincipal currentUser);
    ResultResponseDTO list(String email, Integer page, Integer pageSize, UserPrincipal currentUser);
}
