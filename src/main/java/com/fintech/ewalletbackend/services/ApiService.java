package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.ApiRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface ApiService {
    ResultResponseDTO create(ApiRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO update(String id, ApiRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO info(String id, UserPrincipal currentUser);

    ResultResponseDTO delete(String id, UserPrincipal currentUser);

    ResultResponseDTO list(String apiName, String name, Integer page, Integer pageSize, UserPrincipal currentUser);
}
