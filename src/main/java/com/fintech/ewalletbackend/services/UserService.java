package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.UserRequestDTO;
import com.fintech.ewalletbackend.DTOs.request.UserUpdateRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.enums.StatusEnum;

public interface UserService {
    ResultResponseDTO create(UserRequestDTO dto, String role, StatusEnum status, UserPrincipal currentUser);

    ResultResponseDTO update(String updateFunction, String id, UserUpdateRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO info(String id, UserPrincipal currentUser);

    ResultResponseDTO delete(String id, UserPrincipal currentUser);
}
