package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.LoginRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;

import javax.servlet.http.HttpServletRequest;

public interface AuthenticationService {
    ResultResponseDTO login(LoginRequestDTO dto, HttpServletRequest request);
}
