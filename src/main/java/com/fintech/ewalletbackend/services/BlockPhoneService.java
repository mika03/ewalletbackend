package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.BlockPhoneRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface BlockPhoneService {
    ResultResponseDTO create(BlockPhoneRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO update(String id, BlockPhoneRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO info(String id, UserPrincipal currentUser);

    ResultResponseDTO delete(String id, UserPrincipal currentUser);

    ResultResponseDTO list(String phone, Integer page, Integer pageSize, UserPrincipal currentUser);
}
