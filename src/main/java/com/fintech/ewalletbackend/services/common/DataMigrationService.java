package com.fintech.ewalletbackend.services.common;

import com.fintech.ewalletbackend.entities.Role;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.StatusEnum;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class DataMigrationService extends BaseService {

    @PostConstruct
    private void changeSet() {
        initData();
    }

    private void initData() {
        User admin = userRepository.findByUsername("admin");
        if (admin == null) {
            List<String> roles = new ArrayList<>();
            roles.add("ADMIN");
            User user = new User();
            user.setName("Admin");
            user.setUsername("admin");
            user.setEmail("admin@gmail.com");
            user.setPhone("0389718787");
            user.setPassword(pwdEnc.encode("123456"));
            user.setRoles(roles);
            user.setStatus(StatusEnum.ACTIVE);
            user.setCreatedDate(LocalDateTime.now());
            user.setIsDeleted(false);
            userRepository.save(user);
        }

        User user1 = userRepository.findByUsername("user1");
        if (user1 == null) {
            List<String> roles = new ArrayList<>();
            roles.add("USER");
            User user = new User();
            user.setName("User 1");
            user.setUsername("user1");
            user.setEmail("user1@gmail.com");
            user.setPhone("0389718787");
            user.setPassword(pwdEnc.encode("123456"));
            user.setRoles(roles);
            user.setStatus(StatusEnum.ACTIVE);
            user.setCreatedDate(LocalDateTime.now());
            user.setIsDeleted(false);
            userRepository.save(user);
        }

        Role roleAdmin = roleRepository.findByName("ADMIN");
        if (roleAdmin == null) {
            Role role = new Role();
            role.setName("ADMIN");
            role.setDescription("ADMIN");
            role.setNote("This role is not deleted");
            role.setIsDeleted(false);
            role.setStatus(StatusEnum.ACTIVE);
            roleRepository.save(role);
        }

        Role roleAdminLevel1 = roleRepository.findByName("ADMIN_LEVEL_1");
        if (roleAdminLevel1 == null) {
            Role role = new Role();
            role.setName("ADMIN_LEVEL_1");
            role.setDescription("ADMIN_LEVEL_1");
            role.setNote("This role is not deleted");
            role.setStatus(StatusEnum.ACTIVE);
            role.setIsDeleted(false);
            roleRepository.save(role);
        }

        Role roleAdminLevel2 = roleRepository.findByName("ADMIN_LEVEL_2");
        if (roleAdminLevel2 == null) {
            Role role = new Role();
            role.setName("ADMIN_LEVEL_2");
            role.setDescription("ADMIN_LEVEL_2");
            role.setNote("This role is not deleted");
            role.setStatus(StatusEnum.ACTIVE);
            role.setIsDeleted(false);
            roleRepository.save(role);
        }

        Role roleUser = roleRepository.findByName("USER");
        if (roleUser == null) {
            Role role = new Role();
            role.setName("USER");
            role.setDescription("USER");
            role.setNote("This role is not deleted");
            role.setStatus(StatusEnum.ACTIVE);
            role.setIsDeleted(false);
            roleRepository.save(role);
        }
    }
}
