package com.fintech.ewalletbackend.services.common;

import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.*;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.repositories.*;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public abstract class BaseService {
    @Autowired
    protected CurrencyRepository currencyRepository;
    @Autowired
    protected MerchantCurrencyRepository merchantCurrencyRepository;
    @Autowired
    protected MerchantRepository merchantRepository;
    @Autowired
    protected OrderRepository orderRepository;
    @Autowired
    protected PaymentRepository paymentRepository;
    @Autowired
    protected TransactionRepository transactionRepository;
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected WalletRepository walletRepository;
    @Autowired
    protected RefreshTokenRepository refreshTokenRepository;
    @Autowired
    protected RoleRepository roleRepository;

    @Autowired
    protected BlockEmailRepository blockEmailRepository;
    @Autowired
    protected BlockPhoneRepository blockPhoneRepository;
    @Autowired
    protected DeviceRepository deviceRepository;
    @Autowired
    protected ApiRepository apiRepository;

    @Autowired
    protected SystemPermissionRepository systemPermissionRepository;
    @Autowired
    protected LogRepository logRepository;
    @Autowired
    protected DozerBeanMapper mapper;
    @Autowired
    protected PasswordEncoder pwdEnc;

    protected ResultResponseDTO checkUser(UserPrincipal currentUser) {
        User user = userRepository.getById(new ObjectId(currentUser.getId()));
        if (user == null)
            return new ResultResponseDTO(ResultEnum.UNAUTHORIZED.getCode(), ResultEnum.UNAUTHORIZED.getStatus(), ResultEnum.UNAUTHORIZED.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), user);
    }
}
