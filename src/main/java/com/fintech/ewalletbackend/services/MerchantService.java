package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.MerchantRequestDTO;
import com.fintech.ewalletbackend.DTOs.request.MerchantUpdateRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

import java.security.NoSuchAlgorithmException;

public interface MerchantService {
    ResultResponseDTO create(MerchantRequestDTO dto, UserPrincipal currentUser) throws NoSuchAlgorithmException;

    ResultResponseDTO update(String id, MerchantUpdateRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO info(String id, UserPrincipal currentUser);

    ResultResponseDTO delete(String id, UserPrincipal currentUser);

    ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser);
}
