package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.DeviceRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface DeviceService {
    ResultResponseDTO create(DeviceRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO update(String id, DeviceRequestDTO dto, UserPrincipal currentUser);

    ResultResponseDTO info(String id, UserPrincipal currentUser);

    ResultResponseDTO delete(String id, UserPrincipal currentUser);

    ResultResponseDTO list(String name, String ip, Integer page, Integer pageSize, UserPrincipal currentUser);
}
