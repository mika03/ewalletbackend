package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.PaymentRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface PaymentService {
    ResultResponseDTO create(PaymentRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO update(String id, PaymentRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO info(String id, UserPrincipal currentUser);
    ResultResponseDTO delete(String id, UserPrincipal currentUser);
    ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser);
}
