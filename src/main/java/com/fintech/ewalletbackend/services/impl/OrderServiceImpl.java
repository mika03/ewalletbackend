package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.OrderRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.OrderListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.OrderResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Order;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.OrderService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl extends BaseService implements OrderService {

    @Override
    public ResultResponseDTO create(OrderRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (orderRepository.getByCode(dto.getCode()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        Order order = mapper.map(dto, Order.class);
        order.setCreatedDate(LocalDateTime.now());
        order.setCreatedUser(user.getId());
        order = orderRepository.save(order);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(order, OrderResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, OrderRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        Order order = orderRepository.getById(new ObjectId(id));
        if (order == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        order.setCode(dto.getCode());
        order.setName(dto.getName());
        order.setPrice(dto.getPrice());
        order.setUpdatedDate(LocalDateTime.now());
        order.setUpdatedUser(user.getId());
        order = orderRepository.save(order);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(order, OrderResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Order order = orderRepository.getById(new ObjectId(id));
        if (order == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(order, OrderResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Order order = orderRepository.getById(new ObjectId(id));
        if (order == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        orderRepository.delete(order);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);

        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<OrderResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        Page<Order> orderPage = orderRepository.findAll(pageable);
        for (Order order : orderPage)
            dtoList.add(mapper.map(order, OrderResponseDTO.class));

        totalPage = orderPage.getTotalPages();
        totalSize = orderPage.getTotalElements();

        OrderListResponseDTO orderListResponseDTO = new OrderListResponseDTO();
        orderListResponseDTO.setListData(dtoList);
        orderListResponseDTO.setTotalPage(totalPage);
        orderListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), orderListResponseDTO);
    }
}
