package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.PaymentRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.PaymentListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.PaymentResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Payment;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.PaymentService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentServiceImpl extends BaseService implements PaymentService {

    @Override
    public ResultResponseDTO create(PaymentRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (userRepository.getById(new ObjectId(dto.getUserId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (orderRepository.getById(new ObjectId(dto.getOrderId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        Payment payment = mapper.map(dto, Payment.class);
        payment.setCreatedDate(LocalDateTime.now());
        payment.setCreatedUser(user.getId());
        payment = paymentRepository.save(payment);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(payment, PaymentResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, PaymentRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (userRepository.getById(new ObjectId(dto.getUserId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (orderRepository.getById(new ObjectId(dto.getOrderId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        Payment payment = paymentRepository.getById(new ObjectId(id));
        if (payment == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        payment.setUserId(new ObjectId(dto.getUserId()));
        payment.setOrderId(new ObjectId(dto.getOrderId()));
        payment.setSuccessUrl(dto.getSuccessUrl());
        payment.setCancelUrl(dto.getCancelUrl());
        payment.setDescription(dto.getDescription());
        payment.setNote(dto.getNote());
        payment.setUpdatedDate(LocalDateTime.now());
        payment.setUpdatedUser(user.getId());
        payment = paymentRepository.save(payment);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(payment, PaymentResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Payment payment = paymentRepository.getById(new ObjectId(id));
        if (payment == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(payment, PaymentResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Payment payment = paymentRepository.getById(new ObjectId(id));
        if (payment == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        paymentRepository.delete(payment);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);

        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<PaymentResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        Page<Payment> paymentPage = paymentRepository.findAll(pageable);
        for (Payment payment : paymentPage)
            dtoList.add(mapper.map(payment, PaymentResponseDTO.class));

        totalPage = paymentPage.getTotalPages();
        totalSize = paymentPage.getTotalElements();

        PaymentListResponseDTO paymentListResponseDTO = new PaymentListResponseDTO();
        paymentListResponseDTO.setListData(dtoList);
        paymentListResponseDTO.setTotalPage(totalPage);
        paymentListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), paymentListResponseDTO);
    }
}
