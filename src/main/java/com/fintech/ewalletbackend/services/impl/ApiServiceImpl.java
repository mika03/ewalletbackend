package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.ApiRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ApiListResponse;
import com.fintech.ewalletbackend.DTOs.response.ApiResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Api;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.enums.StatusEnum;
import com.fintech.ewalletbackend.services.ApiService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ApiServiceImpl extends BaseService implements ApiService {
    @Override
    public ResultResponseDTO create(ApiRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (apiRepository.findByApiName(dto.getApiName()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        Api api = mapper.map(dto, Api.class);
        api.setCreatedDate(LocalDateTime.now());
        api.setCreatedUser(user.getId());
        api.setStatus(StatusEnum.ACTIVE);
        api = apiRepository.save(api);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(api, ApiResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, ApiRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        Api api = apiRepository.getById(new ObjectId(id));
        if (api == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        api.setApiName(dto.getApiName());
        api.setName(dto.getName());
        api.setDescription(dto.getDescription());
        api.setNote(dto.getNote());
        api.setUpdatedDate(LocalDateTime.now());
        api.setUpdatedUser(user.getId());
        api = apiRepository.save(api);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(api, ApiResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Api api = apiRepository.getById(new ObjectId(id));
        if (api == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(api, ApiResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Api api = apiRepository.getById(new ObjectId(id));
        if (api == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        apiRepository.delete(api);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(String apiName, String name, Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<ApiResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        if (apiName == null && name == null) {
            Page<Api> apiPage = apiRepository.findAll(pageable);
            for (Api api : apiPage)
                dtoList.add(mapper.map(api, ApiResponseDTO.class));

            totalPage = apiPage.getTotalPages();
            totalSize = apiPage.getTotalElements();
        } else {
            Page<Api> apiPage = apiRepository.findByApiNameOrName(apiName, name, pageable);
            for (Api api : apiPage)
                dtoList.add(mapper.map(api, ApiResponseDTO.class));

            totalPage = apiPage.getTotalPages();
            totalSize = apiPage.getTotalElements();
        }

        ApiListResponse apiListResponse = new ApiListResponse();
        apiListResponse.setListData(dtoList);
        apiListResponse.setTotalPage(totalPage);
        apiListResponse.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), apiListResponse);
    }
}
