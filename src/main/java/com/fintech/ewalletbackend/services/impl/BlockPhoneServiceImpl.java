package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.BlockPhoneRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.BlockPhoneListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.BlockPhoneResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.BlockPhone;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.BlockEnum;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.BlockPhoneService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlockPhoneServiceImpl extends BaseService implements BlockPhoneService {

    @Override
    public ResultResponseDTO create(BlockPhoneRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        if (blockPhoneRepository.findByPhone(dto.getPhone()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        BlockPhone blockPhone = mapper.map(dto, BlockPhone.class);
        blockPhone.setIsBlocked(BlockEnum.BLOCKED);
        blockPhone.setCreatedDate(LocalDateTime.now());
        blockPhone.setCreatedUser(user.getId());
        blockPhone = blockPhoneRepository.save(blockPhone);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(blockPhone, BlockPhoneResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, BlockPhoneRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        BlockPhone blockPhone = blockPhoneRepository.getById(new ObjectId(id));
        if (blockPhone == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        blockPhone.setPhone(dto.getPhone());
        blockPhone.setNote(dto.getNote());
        blockPhone.setDescription(dto.getDescription());
        blockPhone.setUpdatedDate(LocalDateTime.now());
        blockPhone.setUpdatedUser(user.getId());
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(blockPhone, BlockPhoneResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        BlockPhone blockPhone = blockPhoneRepository.getById(new ObjectId(id));
        if (blockPhone == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(blockPhone, BlockPhoneResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        BlockPhone blockPhone = blockPhoneRepository.getById(new ObjectId(id));
        if (blockPhone == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        blockPhoneRepository.delete(blockPhone);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(String phone, Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<BlockPhoneResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        if (phone == null) {
            Page<BlockPhone> blockPhonePage = blockPhoneRepository.findAll(pageable);
            for (BlockPhone blockPhone : blockPhonePage)
                dtoList.add(mapper.map(blockPhone, BlockPhoneResponseDTO.class));

            totalPage = blockPhonePage.getTotalPages();
            totalSize = blockPhonePage.getTotalElements();
        } else {
            Page<BlockPhone> blockPhonePage = blockPhoneRepository.getByPhone(phone, pageable);
            for (BlockPhone blockPhone : blockPhonePage)
                dtoList.add(mapper.map(blockPhone, BlockPhoneResponseDTO.class));

            totalPage = blockPhonePage.getTotalPages();
            totalSize = blockPhonePage.getTotalElements();
        }

        BlockPhoneListResponseDTO blockPhoneListResponseDTO = new BlockPhoneListResponseDTO();
        blockPhoneListResponseDTO.setListData(dtoList);
        blockPhoneListResponseDTO.setTotalPage(totalPage);
        blockPhoneListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), blockPhoneListResponseDTO);
    }
}
