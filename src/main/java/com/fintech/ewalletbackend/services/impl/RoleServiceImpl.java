package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.RoleRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.RoleListDTO;
import com.fintech.ewalletbackend.DTOs.response.RoleResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Role;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.RoleService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl extends BaseService implements RoleService {
    @Override
    public ResultResponseDTO create(RoleRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();
        if (roleRepository.findByName(dto.getName()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        Role role = mapper.map(dto, Role.class);
        role.setCreatedUser(user.getId());
        role.setCreatedDate(LocalDateTime.now());
        role = roleRepository.save(role);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(role, RoleResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, RoleRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();
        Role role = roleRepository.getById(new ObjectId(id));
        if (role == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        role.setName(dto.getName());
        role.setDescription(dto.getDescription());
        role.setNote(dto.getNote());
        role.setUpdatedDate(LocalDateTime.now());
        role.setUpdatedUser(user.getId());
        role = roleRepository.save(role);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(role, RoleResponseDTO.class));
    }

    @Override
    public ResultResponseDTO list(String name, Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<RoleResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        if (name == null) {
            Page<Role> rolePage = roleRepository.findAll(pageable);
            rolePage.forEach(x -> {
                RoleResponseDTO dto = new RoleResponseDTO();
                dto.setId(x.getId().toString());
                dto.setName(x.getName());
                dto.setDescription(x.getDescription());
                dto.setNote(x.getNote());
                dtoList.add(dto);
            });
            totalPage = rolePage.getTotalPages();
            totalSize = rolePage.getTotalElements();
        } else {
            Page<Role> rolePage = roleRepository.findByName(name, pageable);
            rolePage.forEach(x -> {
                RoleResponseDTO dto = new RoleResponseDTO();
                dto.setId(x.getId().toString());
                dto.setName(x.getName());
                dto.setDescription(x.getDescription());
                dto.setNote(x.getNote());
                dtoList.add(dto);
            });
            totalPage = rolePage.getTotalPages();
            totalSize = rolePage.getTotalElements();
        }
        RoleListDTO roleListDTO = new RoleListDTO();
        roleListDTO.setListData(dtoList);
        roleListDTO.setTotalPage(totalPage);
        roleListDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), roleListDTO);
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        Role role = roleRepository.getById(new ObjectId(id));
        if (role == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(role, RoleResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        Role role = roleRepository.getById(new ObjectId(id));
        if (role == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (role.getIsDeleted() == false)
            return new ResultResponseDTO(ResultEnum.NOT_DELETED.getCode(), ResultEnum.NOT_DELETED.getStatus(), ResultEnum.NOT_DELETED.getMessage(), null);

        roleRepository.delete(role);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

}
