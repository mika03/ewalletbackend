package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.TransactionRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.TransactionListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.TransactionResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Transaction;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.enums.TransactionStatusEnum;
import com.fintech.ewalletbackend.services.TransactionService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionServiceImpl extends BaseService implements TransactionService {

    @Override
    public ResultResponseDTO create(TransactionRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (paymentRepository.getById(new ObjectId(dto.getPaymentId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (walletRepository.getById(new ObjectId(dto.getWalletId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        Transaction transaction = mapper.map(dto, Transaction.class);
        transaction.setCreatedDate(LocalDateTime.now());
        transaction.setCreatedUser(user.getId());
        transaction.setTransactionStatus(TransactionStatusEnum.PENDING);
        transaction = transactionRepository.save(transaction);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(transaction, TransactionResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, TransactionRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (paymentRepository.getById(new ObjectId(dto.getPaymentId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (walletRepository.getById(new ObjectId(dto.getWalletId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        Transaction transaction = transactionRepository.getById(new ObjectId(id));
        if (transaction == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        transaction.setPaymentId(new ObjectId(dto.getPaymentId()));
        transaction.setWalletId(new ObjectId(dto.getWalletId()));
        transaction.setDescription(dto.getDescription());
        transaction.setNote(dto.getNote());
        transaction.setUpdatedDate(LocalDateTime.now());
        transaction.setUpdatedUser(user.getId());
        transaction = transactionRepository.save(transaction);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(transaction, TransactionResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Transaction transaction = transactionRepository.getById(new ObjectId(id));
        if (transaction == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(transaction, TransactionResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Transaction transaction = transactionRepository.getById(new ObjectId(id));
        if (transaction == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        transactionRepository.delete(transaction);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);

        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<TransactionResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;

        Page<Transaction> transactionPage = transactionRepository.findAll(pageable);
        for (Transaction transaction : transactionPage)
            dtoList.add(mapper.map(transaction, TransactionResponseDTO.class));

        totalPage = transactionPage.getTotalPages();
        totalSize = transactionPage.getTotalElements();

        TransactionListResponseDTO transactionListResponseDTO = new TransactionListResponseDTO();
        transactionListResponseDTO.setListData(dtoList);
        transactionListResponseDTO.setTotalPage(totalPage);
        transactionListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), transactionListResponseDTO);
    }
}
