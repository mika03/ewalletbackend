package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.SystemPermissionRequestDTO;
import com.fintech.ewalletbackend.DTOs.request.SystemPermissionUpdateRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.SystemPermissionListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.SystemPermissionResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.SystemPermission;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.enums.StatusEnum;
import com.fintech.ewalletbackend.services.SystemPermissionService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class SystemPermissionServiceImpl extends BaseService implements SystemPermissionService {

    @Override
    public ResultResponseDTO create(SystemPermissionRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        if (roleRepository.getById(new ObjectId(dto.getRoleId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        if (systemPermissionRepository.findByRoleId(new ObjectId(dto.getRoleId())) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        SystemPermission systemPermission = mapper.map(dto, SystemPermission.class);
        systemPermission.setCreatedDate(LocalDateTime.now());
        systemPermission.setCreatedUser(user.getId());
        systemPermission.setStatus(StatusEnum.ACTIVE);
        systemPermission = systemPermissionRepository.save(systemPermission);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(systemPermission, SystemPermissionResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, SystemPermissionUpdateRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        SystemPermission systemPermission = systemPermissionRepository.getById(new ObjectId(id));
        if (systemPermission == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        systemPermission.setIsCreated(dto.getIsCreated());
        systemPermission.setIsUpdated(dto.getIsUpdated());
        systemPermission.setIsDeleted(dto.getIsDeleted());
        systemPermission.setIsViewed(dto.getIsViewed());
        systemPermission.setDescription(dto.getDescription());
        systemPermission.setNote(dto.getNote());
        systemPermission.setUpdatedDate(LocalDateTime.now());
        systemPermission.setUpdatedUser(user.getId());
        systemPermission = systemPermissionRepository.save(systemPermission);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(systemPermission, SystemPermissionResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        SystemPermission systemPermission = systemPermissionRepository.getById(new ObjectId(id));
        if (systemPermission == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(systemPermission, SystemPermissionResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        SystemPermission systemPermission = systemPermissionRepository.getById(new ObjectId(id));
        if (systemPermission == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        systemPermissionRepository.delete(systemPermission);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(String roleId, String status, Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<SystemPermissionResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        if (roleId == null && status == null) {
            Page<SystemPermission> systemPermissionPage = systemPermissionRepository.findAll(pageable);
            for (SystemPermission systemPermission : systemPermissionPage)
                dtoList.add(mapper.map(systemPermission, SystemPermissionResponseDTO.class));

            totalPage = systemPermissionPage.getTotalPages();
            totalSize = systemPermissionPage.getTotalElements();
        } else {
            StatusEnum statusEnum = StatusEnum.valueOf(status);
            if (statusEnum == null)
                return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

            Page<SystemPermission> systemPermissionPage = systemPermissionRepository.findByRoleIdOrStatus(new ObjectId(roleId), statusEnum, pageable);
            for (SystemPermission systemPermission : systemPermissionPage)
                dtoList.add(mapper.map(systemPermission, SystemPermissionResponseDTO.class));

            totalPage = systemPermissionPage.getTotalPages();
            totalSize = systemPermissionPage.getTotalElements();
        }

        SystemPermissionListResponseDTO systemPermissionListResponseDTO = new SystemPermissionListResponseDTO();
        systemPermissionListResponseDTO.setListData(dtoList);
        systemPermissionListResponseDTO.setTotalPage(totalPage);
        systemPermissionListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), systemPermissionListResponseDTO);
    }
}
