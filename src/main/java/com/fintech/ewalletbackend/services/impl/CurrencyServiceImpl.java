package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.CurrencyRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.CurrencyListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.CurrencyResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Currency;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.CurrencyService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CurrencyServiceImpl extends BaseService implements CurrencyService {

    @Override
    public ResultResponseDTO create(CurrencyRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        Currency currency = mapper.map(dto, Currency.class);
        currency.setCreatedDate(LocalDateTime.now());
        currency.setCreatedUser(user.getId());
        currency = currencyRepository.save(currency);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(currency, CurrencyResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, CurrencyRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        Currency currency = currencyRepository.getById(new ObjectId(id));
        if (currency == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        currency.setName(dto.getName());
        currency.setDescription(dto.getDescription());
        currency.setAbbreviation(dto.getAbbreviation());
        currency.setNote(dto.getNote());
        currency.setUpdatedDate(LocalDateTime.now());
        currency.setCreatedUser(user.getId());
        currency = currencyRepository.save(currency);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(currency, CurrencyResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        Currency currency = currencyRepository.getById(new ObjectId(id));
        if (currency == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(currency, CurrencyResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        Currency currency = currencyRepository.getById(new ObjectId(id));
        if (currency == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        currencyRepository.delete(currency);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<CurrencyResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        Page<Currency> currencyPage = currencyRepository.findAll(pageable);
        for (Currency currency : currencyPage)
            dtoList.add(mapper.map(currency, CurrencyResponseDTO.class));

        totalPage = currencyPage.getTotalPages();
        totalSize = currencyPage.getTotalElements();

        CurrencyListResponseDTO currencyListResponseDTO = new CurrencyListResponseDTO();
        currencyListResponseDTO.setListData(dtoList);
        currencyListResponseDTO.setTotalPage(totalPage);
        currencyListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), currencyListResponseDTO);
    }
}
