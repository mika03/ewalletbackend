package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.WalletRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.WalletListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.WalletResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.entities.Wallet;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.WalletService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class WalletServiceImpl extends BaseService implements WalletService {

    @Override
    public ResultResponseDTO create(WalletRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (userRepository.getById(new ObjectId(dto.getUserId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (currencyRepository.getById(new ObjectId(dto.getCurrencyId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        Wallet wallet = mapper.map(dto, Wallet.class);
        wallet.setCreatedDate(LocalDateTime.now());
        wallet.setCreatedUser(user.getId());
        wallet = walletRepository.save(wallet);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(wallet, WalletResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, WalletRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (userRepository.getById(new ObjectId(dto.getUserId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (currencyRepository.getById(new ObjectId(dto.getCurrencyId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        Wallet wallet = walletRepository.getById(new ObjectId(id));
        if (wallet == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        wallet.setAddress(dto.getAddress());
        wallet.setUserId(new ObjectId(dto.getUserId()));
        wallet.setCurrencyId(new ObjectId(dto.getCurrencyId()));
        wallet.setPrice(dto.getPrice());
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(wallet, WalletResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Wallet wallet = walletRepository.getById(new ObjectId(id));
        if (wallet == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(wallet, WalletResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Wallet wallet = walletRepository.getById(new ObjectId(id));
        if (wallet == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        walletRepository.delete(wallet);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);

        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<WalletResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        Page<Wallet> walletPage = walletRepository.findAll(pageable);
        for (Wallet wallet : walletPage)
            dtoList.add(mapper.map(wallet, WalletResponseDTO.class));

        totalPage = walletPage.getTotalPages();
        totalSize = walletPage.getTotalElements();

        WalletListResponseDTO walletListResponseDTO = new WalletListResponseDTO();
        walletListResponseDTO.setListData(dtoList);
        walletListResponseDTO.setTotalPage(totalPage);
        walletListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), walletListResponseDTO);
    }
}
