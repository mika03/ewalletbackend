package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.UserRequestDTO;
import com.fintech.ewalletbackend.DTOs.request.UserUpdateRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.UserResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.constants.CommonConstant;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.enums.StatusEnum;
import com.fintech.ewalletbackend.services.UserService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends BaseService implements UserService {

    @Override
    public ResultResponseDTO create(UserRequestDTO dto, String role, StatusEnum status, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (userRepository.findByUsernameOrEmailOrPhone(dto.getUsername(), dto.getEmail(), dto.getPhone()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        User data = mapper.map(dto, User.class);
        List<String> roles = new ArrayList<>();
        roles.add(role);
        data.setRoles(roles);
        data.setIsDeleted(true);
        data.setStatus(status);
        data.setCreatedUser(user.getId());
        data.setCreatedDate(LocalDateTime.now());
        data = userRepository.save(data);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(data, UserResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String updateFunction, String id, UserUpdateRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        User data = userRepository.getById(new ObjectId(id));
        if (CommonConstant.FUNC_UPDATE_ADMIN_LEVEL_1.equals(updateFunction))
            checkAdminLevel1(user, data);

        if (CommonConstant.FUNC_UPDATE_ADMIN_LEVEL_2.equals(updateFunction))
            checkAdminLevel2(user, data);

        data.setName(dto.getName());
        data.setUpdatedUser(user.getId());
        data.setUpdatedDate(LocalDateTime.now());
        data = userRepository.save(data);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(data, UserResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        User data = userRepository.getById(new ObjectId(id));
        if (data == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(data, ResultResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        User data = userRepository.getById(new ObjectId(id));
        if (data == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        userRepository.delete(data);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    private ResultResponseDTO checkAdminLevel1(User user, User data) {
        if (data == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (user.getRoles().contains("ADMIN_LEVEL_1") &&
                data.getRoles().contains("ADMIN_LEVEL_1")) {
            if (user.getId().equals(data.getId()))
                return new ResultResponseDTO(ResultEnum.NOT_ACCESSED_ACCOUNT.getCode(), ResultEnum.NOT_ACCESSED_ACCOUNT.getStatus(), ResultEnum.NOT_ACCESSED_ACCOUNT.getMessage(), null);
        }

        return new ResultResponseDTO();
    }

    private ResultResponseDTO checkAdminLevel2(User user, User data) {
        if (data == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (user.getRoles().contains("ADMIN_LEVEL_2") &&
                data.getRoles().contains("ADMIN_LEVEL_2")) {
            if (user.getId().equals(data.getId()))
                return new ResultResponseDTO(ResultEnum.NOT_ACCESSED_ACCOUNT.getCode(), ResultEnum.NOT_ACCESSED_ACCOUNT.getStatus(), ResultEnum.NOT_ACCESSED_ACCOUNT.getMessage(), null);
        }

        return new ResultResponseDTO();
    }

}
