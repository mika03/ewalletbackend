package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.DeviceRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.DeviceListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.DeviceResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Device;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.BlockEnum;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.DeviceService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class DeviceServiceImpl extends BaseService implements DeviceService {

    @Override
    public ResultResponseDTO create(DeviceRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        if (deviceRepository.findByIp(dto.getIp()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        Device device = mapper.map(dto, Device.class);
        device.setCreatedDate(LocalDateTime.now());
        device.setCreatedUser(user.getId());
        device.setIsBlocked(BlockEnum.UNBLOCKED);
        device = deviceRepository.save(device);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(device, DeviceResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, DeviceRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        Device device = deviceRepository.getById(new ObjectId(id));
        if (device == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        device.setIp(dto.getIp());
        device.setName(dto.getName());
        device.setDescription(dto.getDescription());
        device.setName(dto.getNote());
        device.setUpdatedDate(LocalDateTime.now());
        device.setUpdatedUser(user.getId());
        device = deviceRepository.save(device);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(device, DeviceResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        Device device = deviceRepository.getById(new ObjectId(id));
        if (device == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(device, DeviceResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        Device device = deviceRepository.getById(new ObjectId(id));
        if (device == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        deviceRepository.delete(device);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(String name, String ip, Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<DeviceResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        if (name == null && ip == null) {
            Page<Device> devicePage = deviceRepository.findAll(pageable);
            for (Device device : devicePage)
                dtoList.add(mapper.map(device, DeviceResponseDTO.class));

            totalPage = devicePage.getTotalPages();
            totalSize = devicePage.getTotalElements();
        } else {
            Page<Device> devicePage = deviceRepository.findByNameOrIp(name, ip, pageable);
            for (Device device : devicePage)
                dtoList.add(mapper.map(device, DeviceResponseDTO.class));

            totalPage = devicePage.getTotalPages();
            totalSize = devicePage.getTotalElements();
        }

        DeviceListResponseDTO deviceListResponseDTO = new DeviceListResponseDTO();
        deviceListResponseDTO.setListData(dtoList);
        deviceListResponseDTO.setTotalPage(totalPage);
        deviceListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), deviceListResponseDTO);
    }
}
