package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.MerchantRequestDTO;
import com.fintech.ewalletbackend.DTOs.request.MerchantUpdateRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.MerchantListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.MerchantResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.Merchant;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.MerchantService;
import com.fintech.ewalletbackend.services.common.BaseService;
import com.fintech.ewalletbackend.utils.KeyPairUtil;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MerchantServiceImpl extends BaseService implements MerchantService {

    @Override
    public ResultResponseDTO create(MerchantRequestDTO dto, UserPrincipal currentUser) throws NoSuchAlgorithmException {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        if (userRepository.getById(new ObjectId(dto.getUserId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (merchantRepository.findByCode(dto.getCode().toLowerCase().trim()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        Merchant merchant = mapper.map(dto, Merchant.class);
        Map<String, String> keyPair = KeyPairUtil.generateKey();
        merchant.setMerchantAccessKey(keyPair.get("publicKey"));
        merchant.setMerchantSecretKey(keyPair.get("privateKey"));
        merchant.setCreatedDate(LocalDateTime.now());
        merchant.setCreatedUser(user.getId());
        merchant = merchantRepository.save(merchant);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(merchant, MerchantResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, MerchantUpdateRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        Merchant merchant = merchantRepository.getById(new ObjectId(id));
        if (merchant == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        merchant.setName(dto.getName());
        merchant.setNotifyUrl(dto.getNotifyUrl());
        merchant.setWebsite(dto.getWebsite());
        merchant.setUpdatedDate(LocalDateTime.now());
        merchant.setUpdatedUser(user.getId());
        merchant = merchantRepository.save(merchant);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(merchant, MerchantResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Merchant merchant = merchantRepository.getById(new ObjectId(id));
        if (merchant == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(merchant, MerchantResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        Merchant merchant = merchantRepository.getById(new ObjectId(id));
        if (merchant == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        merchantRepository.delete(merchant);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser) {
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<MerchantResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        Page<Merchant> merchantPage = merchantRepository.findAll(pageable);
        for (Merchant merchant : merchantPage)
            dtoList.add(mapper.map(merchant, MerchantResponseDTO.class));

        totalPage = merchantPage.getTotalPages();
        totalSize = merchantPage.getTotalElements();

        MerchantListResponseDTO merchantListResponseDTO = new MerchantListResponseDTO();
        merchantListResponseDTO.setListData(dtoList);
        merchantListResponseDTO.setTotalPage(totalPage);
        merchantListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), merchantListResponseDTO);
    }
}
