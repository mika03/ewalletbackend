package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.LoginRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.TokenResponseDTO;
import com.fintech.ewalletbackend.authentications.TokenProvider;
import com.fintech.ewalletbackend.authentications.TokenUser;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.RefreshToken;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.enums.StatusEnum;
import com.fintech.ewalletbackend.services.AuthenticationService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Service
public class AuthenticationServiceImpl extends BaseService implements AuthenticationService {

    @Autowired
    private TokenProvider tokenProvider;

    @Value("${authentication.token.jwt.expire}")
    private int tokenTimeout;

    @Override
    public ResultResponseDTO login(LoginRequestDTO dto, HttpServletRequest request) {
        User user = userRepository.findByUsernameOrEmailOrPhone(dto.getUsername().toLowerCase(), dto.getUsername().toLowerCase(), dto.getUsername().toLowerCase());
        if (user == null)
            return new ResultResponseDTO(ResultEnum.EMAIL_PASSWORD_INVALID.getCode(), ResultEnum.EMAIL_PASSWORD_INVALID.getStatus(), ResultEnum.EMAIL_PASSWORD_INVALID.getMessage(), null);


        if (StatusEnum.PENDING.equals(user.getStatus()))
            return new ResultResponseDTO(ResultEnum.ACCOUNT_NOT_ACTIVE.getCode(), ResultEnum.ACCOUNT_NOT_ACTIVE.getStatus(), ResultEnum.ACCOUNT_NOT_ACTIVE.getMessage(), null);

        if (!StatusEnum.ACTIVE.equals(user.getStatus()))
            return new ResultResponseDTO(ResultEnum.ACCOUNT_BLOCKED.getCode(), ResultEnum.ACCOUNT_BLOCKED.getStatus(), ResultEnum.ACCOUNT_BLOCKED.getMessage(), null);

        if (!pwdEnc.matches(dto.getPassword(), user.getPassword()))
            return new ResultResponseDTO(ResultEnum.EMAIL_PASSWORD_INVALID.getCode(), ResultEnum.EMAIL_PASSWORD_INVALID.getStatus(), ResultEnum.EMAIL_PASSWORD_INVALID.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), authorizeUser(user));
    }

    private TokenResponseDTO authorizeUser(User user) {
        TokenUser tokenUser = new TokenUser();

        tokenUser.setId(user.getId().toHexString());
        tokenUser.setEmail(user.getEmail());
        List<String> roles = new ArrayList<>(user.getRoles());

        roles.remove(null);
        roles.remove("");
        tokenUser.setAuthorities(roles);
        UserPrincipal userPrincipal = UserPrincipal.create(tokenUser);

        String token = tokenProvider.issueToken(userPrincipal);

        TokenResponseDTO tokenResponseDTO = new TokenResponseDTO();

        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setUserId(user.getId());
        refreshToken.setStatus(StatusEnum.ACTIVE);
        refreshTokenRepository.save(refreshToken);

        tokenResponseDTO.setName(user.getName());
        tokenResponseDTO.setToken(token);
        tokenResponseDTO.setBearerToken("Bearer " + token);
        tokenResponseDTO.setRoles(roles);
        tokenResponseDTO.setRefreshToken(refreshToken.getId().toHexString());
        tokenResponseDTO.setExpire(LocalDateTime.now().plusSeconds(tokenTimeout).toEpochSecond(ZoneOffset.UTC));

        return tokenResponseDTO;
    }
}
