package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.BlockEmailRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.BlockEmailListDTO;
import com.fintech.ewalletbackend.DTOs.response.BlockEmailResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.BlockEmail;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.BlockEnum;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.BlockEmailService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BlockEmailServiceImpl extends BaseService implements BlockEmailService {

    @Override
    public ResultResponseDTO create(BlockEmailRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        if (blockEmailRepository.findByEmail(dto.getEmail()) != null)
            return new ResultResponseDTO(ResultEnum.DATA_EXIST.getCode(), ResultEnum.DATA_EXIST.getStatus(), ResultEnum.DATA_EXIST.getMessage(), null);

        BlockEmail blockEmail = mapper.map(dto, BlockEmail.class);
        blockEmail.setCreatedUser(user.getId());
        blockEmail.setCreatedDate(LocalDateTime.now());
        blockEmail.setIsBlocked(BlockEnum.BLOCKED);
        blockEmail = blockEmailRepository.save(blockEmail);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(blockEmail, BlockEmailResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, BlockEmailRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO userResponse = checkUser(currentUser);
        User user = (User) userResponse.getData();

        BlockEmail blockEmail = blockEmailRepository.findByEmail(dto.getEmail());
        if (blockEmail == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        blockEmail.setEmail(dto.getEmail());
        blockEmail.setDescription(dto.getDescription());
        blockEmail.setNote(dto.getNote());
        blockEmail.setUpdatedDate(LocalDateTime.now());
        blockEmail.setUpdatedUser(user.getId());
        blockEmail = blockEmailRepository.save(blockEmail);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(blockEmail, BlockEmailResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        BlockEmail blockEmail = blockEmailRepository.getById(new ObjectId(id));
        if (blockEmail == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(blockEmail, BlockEmailResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);
        BlockEmail blockEmail = blockEmailRepository.getById(new ObjectId(id));
        if (blockEmail == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        blockEmailRepository.delete(blockEmail);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(String email, Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<BlockEmailResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        if (email == null) {
            Page<BlockEmail> blockEmailPage = blockEmailRepository.findAll(pageable);
            for (BlockEmail blockEmail : blockEmailPage)
                dtoList.add(mapper.map(blockEmail, BlockEmailResponseDTO.class));

            totalPage = blockEmailPage.getTotalPages();
            totalSize = blockEmailPage.getTotalElements();
        } else {
            Page<BlockEmail> blockEmailPage = blockEmailRepository.getByEmail(email, pageable);
            for (BlockEmail blockEmail : blockEmailPage)
                dtoList.add(mapper.map(blockEmail, BlockEmailResponseDTO.class));

            totalPage = blockEmailPage.getTotalPages();
            totalSize = blockEmailPage.getTotalElements();
        }

        BlockEmailListDTO blockEmailListDTO = new BlockEmailListDTO();
        blockEmailListDTO.setListData(dtoList);
        blockEmailListDTO.setTotalPage(totalPage);
        blockEmailListDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), blockEmailListDTO);
    }
}
