package com.fintech.ewalletbackend.services.impl;

import com.fintech.ewalletbackend.DTOs.request.MerchantCurrencyRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.MerchantCurrencyListResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.MerchantCurrencyResponseDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.entities.MerchantCurrency;
import com.fintech.ewalletbackend.entities.User;
import com.fintech.ewalletbackend.enums.ResultEnum;
import com.fintech.ewalletbackend.services.MerchantCurrencyService;
import com.fintech.ewalletbackend.services.common.BaseService;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MerchantCurrencyServiceImpl extends BaseService implements MerchantCurrencyService {

    @Override
    public ResultResponseDTO create(MerchantCurrencyRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (currencyRepository.getById(new ObjectId(dto.getCurrencyId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (merchantRepository.getById(new ObjectId(dto.getMerchantId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        MerchantCurrency merchantCurrency = mapper.map(dto, MerchantCurrency.class);
        merchantCurrency.setCreatedDate(LocalDateTime.now());
        merchantCurrency.setCreatedUser(user.getId());
        merchantCurrency = merchantCurrencyRepository.save(merchantCurrency);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(merchantCurrency, MerchantCurrencyResponseDTO.class));
    }

    @Override
    public ResultResponseDTO update(String id, MerchantCurrencyRequestDTO dto, UserPrincipal currentUser) {
        ResultResponseDTO checkUserResponse = checkUser(currentUser);
        User user = (User) checkUserResponse.getData();

        if (currencyRepository.getById(new ObjectId(dto.getCurrencyId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        if (merchantRepository.getById(new ObjectId(dto.getMerchantId())) == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        MerchantCurrency merchantCurrency = merchantCurrencyRepository.getById(new ObjectId(id));
        if (merchantCurrency == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        merchantCurrency.setCurrencyId(new ObjectId(dto.getCurrencyId()));
        merchantCurrency.setMerchantId(new ObjectId(dto.getMerchantId()));
        merchantCurrency.setUpdatedDate(LocalDateTime.now());
        merchantCurrency.setUpdatedUser(user.getId());
        merchantCurrency = merchantCurrencyRepository.save(merchantCurrency);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(merchantCurrency, MerchantCurrencyResponseDTO.class));
    }

    @Override
    public ResultResponseDTO info(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        MerchantCurrency merchantCurrency = merchantCurrencyRepository.getById(new ObjectId(id));
        if (merchantCurrency == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);


        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), mapper.map(merchantCurrency, MerchantCurrencyResponseDTO.class));
    }

    @Override
    public ResultResponseDTO delete(String id, UserPrincipal currentUser) {
        checkUser(currentUser);

        MerchantCurrency merchantCurrency = merchantCurrencyRepository.getById(new ObjectId(id));
        if (merchantCurrency == null)
            return new ResultResponseDTO(ResultEnum.DATA_NOT_EXIST.getCode(), ResultEnum.DATA_NOT_EXIST.getStatus(), ResultEnum.DATA_NOT_EXIST.getMessage(), null);

        merchantCurrencyRepository.delete(merchantCurrency);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), null);
    }

    @Override
    public ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser) {
        checkUser(currentUser);
        Pageable pageable = PageRequest.of(page, pageSize, Sort.Direction.DESC, "createdDate");
        List<MerchantCurrencyResponseDTO> dtoList = new ArrayList<>();
        int totalPage = 0;
        long totalSize = 0;
        Page<MerchantCurrency> merchantPage = merchantCurrencyRepository.findAll(pageable);
        for (MerchantCurrency merchantCurrency : merchantPage)
            dtoList.add(mapper.map(merchantCurrency, MerchantCurrencyResponseDTO.class));

        totalPage = merchantPage.getTotalPages();
        totalSize = merchantPage.getTotalElements();

        MerchantCurrencyListResponseDTO merchantCurrencyListResponseDTO = new MerchantCurrencyListResponseDTO();
        merchantCurrencyListResponseDTO.setListData(dtoList);
        merchantCurrencyListResponseDTO.setTotalPage(totalPage);
        merchantCurrencyListResponseDTO.setTotalSize(totalSize);
        return new ResultResponseDTO(ResultEnum.SUCCESS.getCode(), ResultEnum.SUCCESS.getStatus(), ResultEnum.SUCCESS.getMessage(), merchantCurrencyListResponseDTO);
    }
}
