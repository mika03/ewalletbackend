package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.CurrencyRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface CurrencyService {
    ResultResponseDTO create(CurrencyRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO update(String id, CurrencyRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO info(String id, UserPrincipal currentUser);
    ResultResponseDTO delete(String id, UserPrincipal currentUser);
    ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser);
}
