package com.fintech.ewalletbackend.services;

import com.fintech.ewalletbackend.DTOs.request.MerchantCurrencyRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;

public interface MerchantCurrencyService {
    ResultResponseDTO create(MerchantCurrencyRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO update(String id, MerchantCurrencyRequestDTO dto, UserPrincipal currentUser);
    ResultResponseDTO info(String id, UserPrincipal currentUser);
    ResultResponseDTO delete(String id, UserPrincipal currentUser);
    ResultResponseDTO list(Integer page, Integer pageSize, UserPrincipal currentUser);
}
