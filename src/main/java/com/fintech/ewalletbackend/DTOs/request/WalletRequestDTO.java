package com.fintech.ewalletbackend.DTOs.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WalletRequestDTO {
    private String address;

    private String userId;

    private String currencyId;

    private Long price;
}
