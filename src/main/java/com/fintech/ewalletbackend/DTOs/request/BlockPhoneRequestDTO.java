package com.fintech.ewalletbackend.DTOs.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BlockPhoneRequestDTO {
    private String phone;

    private String deviceId;

    private String description;

    private String note;
}
