package com.fintech.ewalletbackend.DTOs.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SystemPermissionUpdateRequestDTO {
    private Boolean isCreated;

    private Boolean isUpdated;

    private Boolean isDeleted;

    private Boolean isViewed;

    private String description;

    private String note;
}
