package com.fintech.ewalletbackend.DTOs.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MerchantCurrencyListResponseDTO {
    private List<MerchantCurrencyResponseDTO> listData;

    private int totalPage;

    private long totalSize;
}
