package com.fintech.ewalletbackend.DTOs.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SystemPermissionResponseDTO {
    private String id;

    private String roleId;

    private Boolean isCreated;

    private Boolean isUpdated;

    private Boolean isDeleted;

    private Boolean isViewed;

    private String description;

    private String note;
}
