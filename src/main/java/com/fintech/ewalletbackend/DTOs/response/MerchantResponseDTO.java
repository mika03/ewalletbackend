package com.fintech.ewalletbackend.DTOs.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MerchantResponseDTO {
    private String id;

    private String code;

    private String name;

    private String userId;

    private String notifyUrl;

    private String website;

    private String merchantAccessKey;

    private String merchantSecretKey;
}
