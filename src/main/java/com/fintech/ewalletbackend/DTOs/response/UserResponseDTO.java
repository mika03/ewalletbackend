package com.fintech.ewalletbackend.DTOs.response;

import com.fintech.ewalletbackend.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponseDTO {
    private String id;

    private String name;

    private String username;

    private String password;

    private String email;

    private String phone;

    private List<String> roles;

    private StatusEnum status;
}
