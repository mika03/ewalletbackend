package com.fintech.ewalletbackend.DTOs.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResultResponseDTO<T> {
    private String code;
    private String status;
    private String message;
    private T data;
}
