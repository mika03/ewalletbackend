package com.fintech.ewalletbackend.DTOs.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentResponseDTO {
    private String id;

    private String userId;

    private String orderId;

    private String successUrl;

    private String cancelUrl;

    private String description;

    private String note;
}
