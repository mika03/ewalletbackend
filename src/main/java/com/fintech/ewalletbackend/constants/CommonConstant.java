package com.fintech.ewalletbackend.constants;

public class CommonConstant {
    // Function
    public static String FUNC_UPDATE_ADMIN_LEVEL_1 = "updateAdminLevel1";
    public static String FUNC_UPDATE_ADMIN_LEVEL_2 = "updateAdminLevel2";
    public static String FUNC_UPDATE_USER = "updateUser";
    public static String FUNC_UPDATE_ADMIN = "updateAdmin";

    // Api
    public static String API_AUTHENTICATION_LOGIN = "/authentication/login";

    public static String API_BLOCK_EMAIL_CREATE = "";
    public static String API_BLOCK_EMAIL_UPDATE = "";
    public static String API_BLOCK_EMAIL_INFO = "";
    public static String API_BLOCK_EMAIL_DELETE = "";
    public static String API_BLOCK_EMAIL_LIST = "";

    public static String API_BLOCK_PHONE_CREATE = "";
    public static String API_BLOCK_PHONE_UPDATE = "";
    public static String API_BLOCK_PHONE_INFO = "";
    public static String API_BLOCK_PHONE_DELETE = "";
    public static String API_BLOCK_PHONE_LIST = "";

    public static String API_DEVICE_CREATE = "";
    public static String API_DEVICE_UPDATE = "";
    public static String API_DEVICE_INFO = "";
    public static String API_DEVICE_DELETE = "";
    public static String API_DEVICE_LIST = "";

    public static String API_ROLE_CREATE = "";
    public static String API_ROLE_UPDATE = "";
    public static String API_ROLE_INFO = "";
    public static String API_ROLE_DELETE = "";
    public static String API_ROLE_LIST = "";
}
