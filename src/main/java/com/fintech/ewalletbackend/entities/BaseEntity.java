package com.fintech.ewalletbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseEntity implements Serializable {
    protected LocalDateTime createdDate;

    protected LocalDateTime updatedDate;

    protected ObjectId createdUser;

    protected ObjectId updatedUser;
}
