package com.fintech.ewalletbackend.entities;

import com.fintech.ewalletbackend.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Role")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role extends BaseEntity {
    @Id
    private ObjectId id;

    private String name;

    private String description;

    private String note;

    private StatusEnum status;

    private Boolean isDeleted;
}
