package com.fintech.ewalletbackend.entities;

import com.fintech.ewalletbackend.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "API")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Api extends BaseEntity {
    @Id
    private ObjectId id;

    private String apiName;

    private String name;

    private String description;

    private String note;

    private StatusEnum status;
}
