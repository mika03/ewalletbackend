package com.fintech.ewalletbackend.entities;

import com.fintech.ewalletbackend.enums.ConfirmedTransactionEnum;
import com.fintech.ewalletbackend.enums.TransactionStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Transaction")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transaction extends BaseEntity {
    @Id
    private ObjectId id;

    private ObjectId paymentId;

    private ObjectId walletId;

    private String description;

    private String note;

    private Long transactionFee;

    private TransactionStatusEnum transactionStatus;

    private ConfirmedTransactionEnum confirmedTransaction;
}
