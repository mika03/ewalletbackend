package com.fintech.ewalletbackend.entities;

import com.fintech.ewalletbackend.enums.BlockEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Device")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Device extends BaseEntity {
    @Id
    private ObjectId id;

    private String name;

    private String ip;

    private String description;

    private String note;

    private BlockEnum isBlocked;
}
