package com.fintech.ewalletbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MerchantCurrency")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MerchantCurrency extends BaseEntity {
    @Id
    private ObjectId id;

    private ObjectId merchantId;

    private ObjectId currencyId;
}
