package com.fintech.ewalletbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Log")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Log extends BaseEntity {
    @Id
    private ObjectId id;

    private String className;

    private String functionName;

    private Object object;
}
