package com.fintech.ewalletbackend.entities;

import com.fintech.ewalletbackend.enums.BlockEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "BlockEmail")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BlockEmail extends BaseEntity {
    @Id
    private ObjectId id;

    private String email;

    private String description;

    private String note;

    private BlockEnum isBlocked;
}
