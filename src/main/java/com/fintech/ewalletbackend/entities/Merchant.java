package com.fintech.ewalletbackend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Merchant")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Merchant extends BaseEntity {
    @Id
    private ObjectId id;

    private String code;

    private String name;

    private ObjectId userId;

    private String notifyUrl;

    private String website;

    private String merchantAccessKey;

    private String merchantSecretKey;
}
