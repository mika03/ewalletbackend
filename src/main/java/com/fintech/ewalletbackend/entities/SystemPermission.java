package com.fintech.ewalletbackend.entities;

import com.fintech.ewalletbackend.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SystemPermission")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SystemPermission extends BaseEntity {
    @Id
    private ObjectId id;

    private ObjectId roleId;

    private Boolean isCreated;

    private Boolean isUpdated;

    private Boolean isDeleted;

    private Boolean isViewed;

    private String description;

    private String note;

    private StatusEnum status;
}
