package com.fintech.ewalletbackend.entities;

import com.fintech.ewalletbackend.enums.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "User")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity {
    @Id
    private ObjectId id;

    private String name;

    private String username;

    private String password;

    private String email;

    private String phone;

    private List<String> roles;

    private StatusEnum status;

    private Boolean isDeleted;
}
