package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Api;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiRepository extends MongoRepository<Api, ObjectId> {
    Api findByApiName(String apiName);

    Api getById(ObjectId id);
    Page<Api> findByApiNameOrName(String apiName, String name, Pageable pageable);
}
