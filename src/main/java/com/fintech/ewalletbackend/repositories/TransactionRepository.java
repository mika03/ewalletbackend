package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Transaction;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, ObjectId> {
    Transaction getById(ObjectId id);
}
