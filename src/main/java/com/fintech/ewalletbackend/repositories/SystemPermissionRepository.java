package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.SystemPermission;
import com.fintech.ewalletbackend.enums.StatusEnum;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SystemPermissionRepository extends MongoRepository<SystemPermission, ObjectId> {
    SystemPermission getById(ObjectId id);

    SystemPermission findByRoleId(ObjectId roleId);

    Page<SystemPermission> findByRoleIdOrStatus(ObjectId roleId, StatusEnum status, Pageable pageable);
}
