package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Order;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends MongoRepository<Order, ObjectId> {
    Order getById(ObjectId id);

    Order getByCode(String code);
}
