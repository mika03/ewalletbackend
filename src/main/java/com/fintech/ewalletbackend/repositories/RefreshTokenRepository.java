package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.RefreshToken;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RefreshTokenRepository extends MongoRepository<RefreshToken, ObjectId> {
}
