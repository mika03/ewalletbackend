package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.BlockPhone;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockPhoneRepository extends MongoRepository<BlockPhone, ObjectId> {
    BlockPhone findByPhone(String phone);
    BlockPhone getById(ObjectId id);
    Page<BlockPhone> getByPhone(String phone, Pageable pageable);
}
