package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Payment;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends MongoRepository<Payment, ObjectId> {
    Payment getById(ObjectId id);
}
