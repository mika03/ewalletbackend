package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.BlockEmail;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockEmailRepository extends MongoRepository<BlockEmail, ObjectId> {
    BlockEmail findByEmail(String email);
    BlockEmail getById(ObjectId id);
    Page<BlockEmail> getByEmail(String email, Pageable pageable);
}
