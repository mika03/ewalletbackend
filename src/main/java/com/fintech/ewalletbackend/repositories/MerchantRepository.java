package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Merchant;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRepository extends MongoRepository<Merchant, ObjectId> {
    Merchant findByCode(String code);
    Merchant getById(ObjectId id);
}
