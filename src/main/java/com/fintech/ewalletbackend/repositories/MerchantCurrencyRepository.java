package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.MerchantCurrency;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantCurrencyRepository extends MongoRepository<MerchantCurrency, ObjectId> {
    MerchantCurrency getById(ObjectId id);
}
