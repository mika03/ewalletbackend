package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Role;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends MongoRepository<Role, ObjectId> {
    Role findByName(String name);

    Role getById(ObjectId id);

    Page<Role> findByName(String name, Pageable pageable);
}
