package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, ObjectId> {
    User findByUsername(String username);
    User findByUsernameOrEmailOrPhone(String username, String email, String phone);
    User getById(ObjectId id);
}
