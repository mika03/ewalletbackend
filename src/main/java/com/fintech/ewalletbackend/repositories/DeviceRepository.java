package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Device;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends MongoRepository<Device, ObjectId> {
    Device findByIp(String ip);
    Device getById(ObjectId id);
    Page<Device> findByNameOrIp(String name, String ip, Pageable pageable);
}
