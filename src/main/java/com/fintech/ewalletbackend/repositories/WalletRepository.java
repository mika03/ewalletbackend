package com.fintech.ewalletbackend.repositories;

import com.fintech.ewalletbackend.entities.Wallet;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends MongoRepository<Wallet, ObjectId> {
    Wallet getById(ObjectId id);
}
