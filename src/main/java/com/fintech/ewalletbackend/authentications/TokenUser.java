package com.fintech.ewalletbackend.authentications;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TokenUser {
    private String id;

    private String name;

    private String email;

    private Integer expire;

    private List<String> authorities;
}
