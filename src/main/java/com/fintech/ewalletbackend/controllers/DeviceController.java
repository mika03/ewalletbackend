package com.fintech.ewalletbackend.controllers;

import com.fintech.ewalletbackend.DTOs.request.DeviceRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.services.DeviceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/device", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin("*")
@Tag(name = "Device API")
@SecurityRequirement(name = "bearer-key")
public class DeviceController {
    @Autowired
    private DeviceService deviceService;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO create(@RequestBody DeviceRequestDTO dto,
                                    @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return deviceService.create(dto, currentUser);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO update(@PathVariable(name = "id") String id,
                                    @RequestBody DeviceRequestDTO dto,
                                    @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return deviceService.update(id, dto, currentUser);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO delete(@PathVariable(name = "id") String id,
                                    @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return deviceService.delete(id, currentUser);
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @Operation(summary = "Info")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO info(@RequestParam(name = "id") String id,
                                  @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return deviceService.info(id, currentUser);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @Operation(summary = "List")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO list(@RequestParam(name = "name", required = false) String name,
                                  @RequestParam(name = "ip", required = false) String ip,
                                  @RequestParam(name = "page") Integer page,
                                  @RequestParam(name = "pageSize") Integer pageSize,
                                  @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return deviceService.list(name, ip, page, pageSize, currentUser);
    }
}
