package com.fintech.ewalletbackend.controllers;

import com.fintech.ewalletbackend.DTOs.request.ApiRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.services.ApiService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin("*")
@Tag(name = "Api API")
public class ApiController {
    @Autowired
    ApiService apiService;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO create(@RequestBody ApiRequestDTO dto,
                                    @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return apiService.create(dto, currentUser);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO update(@PathVariable(name = "id") String id,
                                    @RequestBody ApiRequestDTO dto,
                                    @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return apiService.update(id, dto, currentUser);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @Operation(summary = "Delete")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO delete(@PathVariable(name = "id") String id,
                                    @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return apiService.delete(id, currentUser);
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @Operation(summary = "Info")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO info(@RequestParam(name = "id") String id,
                                  @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return apiService.info(id, currentUser);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @Operation(summary = "List")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO list(@RequestParam(name = "apiName", required = false) String apiName,
                                  @RequestParam(name = "name", required = false) String name,
                                  @RequestParam(name = "page") Integer page,
                                  @RequestParam(name = "pageSize") Integer pageSize,
                                  @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return apiService.list(apiName, name, page, pageSize, currentUser);
    }
}
