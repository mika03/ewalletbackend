package com.fintech.ewalletbackend.controllers;

import com.fintech.ewalletbackend.DTOs.request.UserRequestDTO;
import com.fintech.ewalletbackend.DTOs.request.UserUpdateRequestDTO;
import com.fintech.ewalletbackend.DTOs.response.ResultResponseDTO;
import com.fintech.ewalletbackend.authentications.UserPrincipal;
import com.fintech.ewalletbackend.constants.CommonConstant;
import com.fintech.ewalletbackend.enums.StatusEnum;
import com.fintech.ewalletbackend.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@CrossOrigin("*")
@Tag(name = "User API")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/create/user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create user")
    @PreAuthorize("hasAuthority('USER')")
    public ResultResponseDTO createUser(@RequestBody UserRequestDTO dto,
                                               @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return userService.create(dto, "USER", StatusEnum.ACTIVE, currentUser);
    }

    @RequestMapping(value = "/create/admin-level-1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create admin level 1")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO createAdminLevel1(@RequestBody UserRequestDTO dto,
                                               @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return userService.create(dto, "ADMIN_LEVEL_1", StatusEnum.ACTIVE, currentUser);
    }

    @RequestMapping(value = "/update/admin-level-1", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update admin level 1")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('ADMIN_LEVEL_1')")
    public ResultResponseDTO updateAdminLevel1(@PathVariable(name = "id") String id,
                                               @RequestBody UserUpdateRequestDTO dto,
                                               @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return userService.update(CommonConstant.FUNC_UPDATE_ADMIN_LEVEL_1, id, dto, currentUser);
    }

    @RequestMapping(value = "/create/admin-level-2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create admin level 2")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('ADMIN_LEVEL_1')")
    public ResultResponseDTO createAdminLevel2(@RequestBody UserRequestDTO dto,
                                               @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return userService.create(dto, "ADMIN_LEVEL_2", StatusEnum.ACTIVE, currentUser);
    }

    @RequestMapping(value = "/update/admin-level-2", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update admin level 2")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('ADMIN_LEVEL_1') or hasAuthority('ADMIN_LEVEL_2')")
    public ResultResponseDTO updateAdminLevel2(@PathVariable(name = "id") String id,
                                               @RequestBody UserUpdateRequestDTO dto,
                                               @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return userService.update(CommonConstant.FUNC_UPDATE_ADMIN_LEVEL_2, id, dto, currentUser);
    }

    @RequestMapping(value = "/update/admin", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Update admin")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResultResponseDTO updateAdmin(@PathVariable(name = "id") String id,
                                         @RequestBody UserUpdateRequestDTO dto,
                                         @Parameter(hidden = true) @AuthenticationPrincipal UserPrincipal currentUser) {
        return userService.update(CommonConstant.FUNC_UPDATE_ADMIN, id, dto, currentUser);
    }
}
