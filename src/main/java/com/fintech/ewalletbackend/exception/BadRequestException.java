package com.fintech.ewalletbackend.exception;

public class BadRequestException extends ServiceException {
    private static final int HTTP_STATUS_CODE = 400;

    public BadRequestException(String message) {
        super(message, HTTP_STATUS_CODE);
    }

    public BadRequestException(String message, int code) {
        super(message, code);
    }
}
